# Skaro server

## Prepare
First you should provision a Docker host (e.g. with [Docker Machine](https://docs.docker.com/machine/)) and install [Docker Compose](https://docs.docker.com/compose/).

## Get an image
Clone this repo
```bash
git clone git@gitlab.com:exarh-team/docker-skaro.git && cd docker-skaro
```
Clone SQL file
```bash
wget https://gitlab.com/exarh-team/skaro-server/raw/master/postgres-setup.sql
```
Build an image
```bash
docker-compose build
```
## Up and run
Start containers with compose
```bash
docker-compose up -d
```
