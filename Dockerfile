FROM debian:latest

RUN apt-get update -qq && apt-get upgrade -qq \
    && apt-get install -y --no-install-recommends \
        nginx-light \
        python3 \
        python3-dev \
        python3-pip \
        libpq-dev \
        build-essential \
        git \
        locales \
        locales-all

ENV LC_ALL en_US.UTF-8
ENV LANG en_US.UTF-8
ENV LANGUAGE en_US.UTF-8

COPY etc/ /etc/

RUN cd /opt \
    && git clone --depth 1 https://gitlab.com/exarh-team/Skaro-WEB.git \
    && git clone --depth 1 https://gitlab.com/exarh-team/skaro-server.git \
    && pip3 install -r /opt/skaro-server/requirements.txt \
    && nginx -t

RUN apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

WORKDIR /opt/skaro-server
